import React, { useState } from "react";

type Props = {};
/**
 *
 * @param param0
 * @returns
 *
 * draft
 *
 * send
 *
 *
 * paid
 *
 */

type InvoiceStatusStep = "draft" | "send" | "paid";

enum InvoiceStatusStep2 {
  DRAFT = "draft",
  SEND = "send",
  PAID = "paid",
}

export default function DemoEnums({}: Props) {
  const [invoiceStatus, setInvoiceStatus] = useState<InvoiceStatusStep2>(
    InvoiceStatusStep2.DRAFT
  );
  return (
    <div className=" container py-10">
      {invoiceStatus == InvoiceStatusStep2.DRAFT && (
        <>
          <h2 className="text-blue-500"> Draft</h2>
          <button
            onClick={() => {
              setInvoiceStatus(InvoiceStatusStep2.SEND);
            }}
            className="bg-blue-500 text-white rounded  px-5 py-2"
          >
            Next
          </button>
        </>
      )}
      {invoiceStatus == InvoiceStatusStep2.SEND && (
        <>
          <h2 className="text-blue-500"> Send</h2>
          <button
            onClick={() => {
              setInvoiceStatus(InvoiceStatusStep2.PAID);
            }}
            className="bg-blue-500 text-white rounded  px-5 py-2"
          >
            Next
          </button>
        </>
      )}
      {invoiceStatus == InvoiceStatusStep2.PAID && (
        <>
          <h2 className="text-blue-500"> Done!</h2>
          <button
            onClick={() => {
              setInvoiceStatus(InvoiceStatusStep2.SEND);
            }}
            className="bg-blue-500 text-white rounded  px-5 py-2"
          >
            Redo
          </button>
        </>
      )}
    </div>
  );
}
