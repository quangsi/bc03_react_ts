import React, { useState } from "react";

type FormType = {
  username: string;
  password: string;
};

let defaultFormValue: FormType = {
  username: "Alice",
  password: "alice123",
};

export default function DemoForm() {
  const [formData, setFormData] = useState<FormType>(defaultFormValue);
  const handleOnchange = (e: React.ChangeEvent<HTMLInputElement>) => {
    console.log(e.target.name, e.target.value);
  };
  return (
    <div>
      <div className="container space-y-5 mt-28 flex flex-col mx-auto">
        <input
          type="text"
          name="username"
          placeholder="username"
          className=" rounded w-full border-blue-400 border-2 "
          onChange={(e) => {
            handleOnchange(e);
          }}
        />
        <input
          type="text"
          name="password"
          placeholder="password"
          className=" w-full rounded border-blue-400 border-2  "
          onChange={(e) => {
            handleOnchange(e);
          }}
        />

        <button className="rounded px-5 py-2 border-black border-2">
          Login
        </button>
      </div>
    </div>
  );
}
