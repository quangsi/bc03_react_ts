import React from "react";
import { TodoListInterface } from "../DemoTodoInterface/DemoTodoInterface";
import ItemTodo from "../ItemTodo/ItemTodo";

export default function ListTodo({ todos }: TodoListInterface) {
  return (
    <div>
      {/*  list */}

      {todos.map((todo) => {
        return (
          <ItemTodo
            // text={todo.text}
            // id={todo.id}
            // isComplete={todo.isComplete}
            todo={todo}
          />
        );
      })}
    </div>
  );
}
