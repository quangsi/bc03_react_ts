import React, { useState } from "react";
import { defaultTodoList } from "./defaultValueTodoList";
import { TodoInterface } from "./DemoTodoInterface/DemoTodoInterface";
import FromTodo from "./FromTodo/FromTodo";
import ListTodo from "./ListTodo/ListTodo";

type Props = {};

export default function DemoTodoList({}: Props) {
  const [todos, setTodos] = useState<TodoInterface[]>(defaultTodoList);

  return (
    <div>
      <FromTodo />
      <ListTodo todos={todos} />
    </div>
  );
}
