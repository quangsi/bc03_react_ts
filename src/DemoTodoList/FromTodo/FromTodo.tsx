import React from "react";

type Props = {};

export default function FromTodo({}: Props) {
  return (
    <div>
      <input type="text" />
      <button className=" rounded px-5 py-2 bg-blue-600 text-white">
        Add Todo
      </button>
    </div>
  );
}
