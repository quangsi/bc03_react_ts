export interface TodoInterface {
  id: number;
  text: string;
  isComplete: boolean;
}

// todo list interface

export interface TodoListInterface {
  todos: TodoInterface[];
}

export interface TodoItemInterface {
  todo: TodoInterface;
}
