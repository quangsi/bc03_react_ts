import { TodoInterface } from "./DemoTodoInterface/DemoTodoInterface";

export const defaultTodoList: TodoInterface[] = [
  { id: 1, text: "Lau nhà", isComplete: false },
  { id: 2, text: "Làm bài tập", isComplete: false },
  { id: 3, text: "Dắt chó đi dạo", isComplete: false },
];
