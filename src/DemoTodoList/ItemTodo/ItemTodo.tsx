import React from "react";
import {
  TodoInterface,
  TodoItemInterface,
} from "../DemoTodoInterface/DemoTodoInterface";

type Props = {};

export default function ItemTodo({ todo }: TodoItemInterface) {
  return (
    <div>
      <p className="text-red-700">{todo.text}</p>
    </div>
  );
}
