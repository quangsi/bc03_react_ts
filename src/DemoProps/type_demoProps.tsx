export type UserAddress = {
  street: string;
  number: number;
};

export type User = {
  name: string;
  age: number;
  address: UserAddress;
};
