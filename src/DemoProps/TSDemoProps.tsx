import React from "react";
import { User } from "./type_demoProps";

type Props = {
  user: User | null;

  title: string;

  handleChangeName?: () => void;
};

export default function TSDemoProps({ user, handleChangeName, title }: Props) {
  return (
    <div className=" p-10 text-white bg-blue-500 my-3">
      <h2>{title}</h2>
      <p>{user?.name}</p>
      <p>{user?.age}</p>
      {/* <p>{user.address}</p> */}

      <button onClick={handleChangeName}>Change name</button>
    </div>
  );
}
