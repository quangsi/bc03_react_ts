import React from "react";
import { Product, UserProfile } from "./interface_demoGeneric";

type Props = {
  dataList: UserProfile[] | Product[];
};

export default function DemoGeneric({ dataList }: Props) {
  return (
    <div className="container mx-auto py-20">
      {dataList.map((item) => {
        return (
          <div>
            {" "}
            {item.id} - {item.name}
          </div>
        );
      })}
    </div>
  );
}
