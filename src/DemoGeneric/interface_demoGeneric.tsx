export interface UserProfile {
  name: string;
  id: number;
}

export interface Product {
  id: number;
  name: string;
  price: number;
}
