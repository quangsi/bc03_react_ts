import React, { useState } from "react";
import logo from "./logo.svg";
import "./App.css";
import TSDemoProps from "./DemoProps/TSDemoProps";
import { User } from "./DemoProps/type_demoProps";
import DemoForm from "./DemoForm/DemoForm";
import DemoEnums from "./DemoEnums/DemoEnums";
import DemoGeneric from "./DemoGeneric/DemoGeneric";
import { Product, UserProfile } from "./DemoGeneric/interface_demoGeneric";
import DemoTodoList from "./DemoTodoList/DemoTodoList";

let userInforAlice = {
  name: "alice",
  age: 18,
  address: {
    street: "Cao Thang",
    number: 112,
  },
};
let userInforBob = {
  name: "Bob",
  // age: 18,
};

let userArr: UserProfile[] = [
  {
    id: 1,
    name: "alice",
  },
  {
    id: 2,
    name: "bob",
  },
  {
    id: 3,
    name: "tom",
  },
];

let productArr: Product[] = [
  {
    id: 1,
    name: "Iphone 11",
    price: 2,
  },
  {
    id: 2,
    name: "iphone 12",
    price: 3,
  },
];
function App() {
  // const [user, setUser] = useState<User | null>(userInforAlice);
  // const [title, setTitle] = useState<string>("Hello");

  // const handleChangeTitle = () => {
  //   setTitle("Hello to alice");
  // };
  return (
    <div className="App">
      {/* <TSDemoProps
        handleChangeName={handleChangeTitle}
        title={title}
        user={user}
      /> */}

      {/* <DemoForm /> */}
      {/* <DemoEnums /> */}
      {/* <DemoGeneric dataList={userArr} />

      <br />
      <DemoGeneric dataList={productArr} /> */}
      <DemoTodoList />
    </div>
  );
}

export default App;
